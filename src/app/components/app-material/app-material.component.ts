import { Component, OnInit, AfterViewInit, OnChanges, ChangeDetectorRef, ChangeDetectionStrategy  } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { Jsonp } from '@angular/http';


@Component({
  selector: 'app-material',
  templateUrl: './app-material.component.html',
  styleUrls: ['./app-material.component.scss'],
  providers: [NgbRatingConfig] // add NgbRatingConfig to the component providers
})
export class AppMaterialComponent implements OnInit {
  public show = true;

  public mobile = false;

  public currentRate = 3;

  constructor(
    private http: HttpClient,
    config: NgbRatingConfig,
    private jsonp: Jsonp,
    public cd: ChangeDetectorRef
  ) {
    // customize default values of ratings used by this component tree
    config.max = 5;
    config.readonly = true;
    // this.cd.detach();  ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked
  }

  images: Array<string>;

  dataStore: any;

  ngOnInit() {
    this.http
      .get('https://picsum.photos/list')
      .pipe(
        map((images: Array<{ id: number }>) => this._randomImageUrls(images))
      )
      .subscribe(images => {
          this.images = images;
        });

    this.getData();
  }

  getRandomArbitrary(): number {
    this.currentRate = Math.floor(Math.random() * (5 - 1)) + 1;
    return this.currentRate;
  }


  getData() {
    // tslint:disable-next-line:max-line-length
    const url = 'https://api.nestoria.co.uk/api?encoding=json&pretty=1&action=search_listings&country=uk&listing_type=buy&place_name=brighton&number_of_results=10&callback=JSONP_CALLBACK';

    this.jsonp.request(url, { method: 'Get' }).subscribe((res: any) => {
      console.log(res._body.response.listings);
      this.dataStore = res._body.response.listings;
      this.dataStore = res._body.response.listings.map(function(x) {
        console.log(x);
         x.ranting =  Math.floor(Math.random() * (5 - 1)) + 1;
         return x;
     });
      console.log(this.dataStore);
    });
  }

  private _randomImageUrls(images: Array<{ id: number }>): Array<string> {
    return [1, 2, 3].map(() => {
      const randomId = images[Math.floor(Math.random() * images.length)].id;
      return `https://picsum.photos/400/300?image=${randomId}`;
    });
  }
}
