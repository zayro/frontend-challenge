import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Interal components
import { HomeComponent } from './components/home/home.component';
import { AppMaterialComponent } from './components/app-material/app-material.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'material', component: AppMaterialComponent },
  { path: '**',  redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

export const RoutingComponents = [HomeComponent, AppMaterialComponent];
