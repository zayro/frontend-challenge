import { Component, OnInit, AfterViewInit, OnChanges, ChangeDetectorRef, ChangeDetectionStrategy  } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { Jsonp } from '@angular/http';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [NgbRatingConfig] // add NgbRatingConfig to the component providers
})
export class HomeComponent implements OnInit {


  // valor inicial del ranking
  public currentRate = 3;

  constructor(
    private http: HttpClient,
    config: NgbRatingConfig,
    private jsonp: Jsonp,
    public cd: ChangeDetectorRef
  ) {

    // customize default values of ratings used by this component tree
    config.max = 5;
    config.readonly = true;

    this.cd.detach();
  }

  images: Array<string>;

  dataStore: any;

  ngOnInit() {

    this.getData();
  }

  // genera un numero aleatorio
  getRandomArbitrary(): number {
    this.currentRate = Math.floor(Math.random() * (5 - 1)) + 1;
    return this.currentRate;
  }

  // obtiene los valores del api con la cantidad de 10 registros
  getData() {
    // tslint:disable-next-line:max-line-length
    const url = 'https://api.nestoria.co.uk/api?encoding=json&pretty=1&action=search_listings&country=uk&listing_type=buy&place_name=brighton&number_of_results=10&callback=JSONP_CALLBACK';

    this.jsonp.request(url, { method: 'Get' }).subscribe((res: any) => {
      // console.log(res._body.response.listings);
      this.dataStore = res._body.response.listings;
      this.cd.detectChanges();
    });
  }


}
