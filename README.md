# Guia Examen Fronted Challenge

### PARTE 1: DESARROLLO EN HTML Y CSS

![Screen](https://gitlab.com/hogar24-challenges/frontend-challenge/raw/master/assets/modelo_frontend.png)

* Crear un nuevo layout basado en la captura dada.
* Añadir hojas de estilo que cumplan a cabalidad el diseño.
* Si tiene conocimientos, añadir un preprocesador de código CSS. *Esto añadirá puntos adicionales*

### PARTE 2: Javascript

* Consumir la API https://api.nestoria.co.uk/api?encoding=json&pretty=1&action=search_listings&country=uk&listing_type=buy&place_name=brighton
para adquirir una lista de propiedades disponibles
* Mostrar los diez primeros anuncios del resultado de la API en el layout creado en la parte 1.
* Por favor usar alguno de los frameworks disponibles (React, AngularJS, VueJS, Ember, Etc.) y por favor justificar por qué hizo uso de ese framework.

## Instrucciones adicionales

* Por favor haga fork a este repositorio
* Tras finalizar, compartir la URL del repositorio con nosotros, puede ser con cualquier proveedor (Gitlab, Github, etc.)
* Los comentarios en el código son esenciales, por favor procurar mantener todo comentado.
* El readme de su proyecto es necesario y fundamental.
* Usar especificación ES6 y superior

## Puntos Adicionales

* Tests
* Código limpio y bien estructurado
* Aplicar las mejores prácticas y patrones de ingeniería del software.
* Uso de frameworks.
* Las medidas de pixeles son perfectas en el diseño.
* Creación y uso de componentes.
* Uso de Lint.
* Uso de conceptos de material design.

## Tabla De Contenido

- [Estructura de carpetas](#Estructura-de-carpetas)
- [Instalacion](#instalacion)
- [Entorno local de desarrollo](Entorno-local-de-desarrollo)
- [Entorno remoto de produccion](Entorno-remoto-de-produccion)
- [Desarrollo local](Desarrollo-local)
- [Scripts-habilitados](Scripts-habilitados)


## Estructura de carpetas

Estructura creada por [Angular CLI](https://github.com/angular/angular-cli) version 6.0.0. y personalizada para la realizacion de la prueba:

```
app/
  documents/
  server/
    data/
    routes/
  src/
    app/
        class/
        components/
        pipe/
    assets/
    config/
    environments/
    services/
```

## Instalacion

se debe tener instalador el software de GIT y NODEJS.

ejecutar el siguiente comando

``` shell
git clone https://gitlab.com/zayro/frontend-challenge.git
```
ingresar al directorio angular y ejecutar el siguiente script

``` shell
npm install
```
instala los componenetes necesarios poder ejecutar los scripts de compilacion y ejecucion.

## Entorno local de desarrollo

Este entorno es en cual desarrollador puede lanzar el aplicativo y realizar modificaciones en tiempo real en el cual la consola le ira mostrando los posibles de igual manera ir configurando los posibles test.

En el directorio raiz del proyecto ejecular los siguientes comandos para arrancar el proyecto.

## Desarrollo local

se ejecuta `ng serve -o`  el comando para navegar en la siguiente url `http://localhost:4200/`. permite ver en vivo los cambias que se van desarrollando.

con este scripts se compilan y manejan los entornos de desarrollo.

#### `npm run develop`

el cual se encargara de compilar el proyecto y subir el servicio web con la siguiente url [http://localhost:3000](http://localhost:3000) 

## Entorno remoto de produccion

El entorno de produccion se encarga conectarse directamente con el api creada que se conecta directamente.

En el directorio raiz del proyecto ejecular los siguientes comandos para arrancar el proyecto.

#### `npm run production`

el cual se encargara de compilar el proyecto y subir el servicio web con la siguiente url [http://localhost:3000](http://localhost:3000) 


### Scripts habilitados

#### `npm test`

Este comando permite observar los componentes en su funcionamiento.

#### `npm lint`

Este comando permite revisar posibles errores de codigo.

#### `npm run serverlocal`

Abrir navegador y agregar la siguiente url [http://localhost:3000](http://localhost:3000) se puede observar que se encuentra corriendo el api local. basado en [express](http://expressjs.com/es/4x/api.html)

## Despliegue entorno production heroku

el aplicativo se testeo con heroku subiendo el proyecto con un deploy en heroku manejando estandares de calidad en el desarrollo.

se puede ver el demo desde:
[https://frontend-challenges.herokuapp.com/](https://frontend-challenges.herokuapp.com/)
<br>
[https://frontend-challenges.herokuapp.com/material](https://frontend-challenges.herokuapp.com/material)
