# Prueba de desarrollo frontend

# Objetivo
Este es un reto de desarrollo frontend que tiene dos propósitos:

1. Mostrar sus capacidades en desarrollo de software, de manera estructurada y siguiendo las mejores prácticas.
2. Entender sus habilidades al trabajar con conjuntos grandes de datos.

## Meta

Para el desarrollo de la prueba se debe demostrar el entendimiento de dos tecnologías esenciales en el desarrollo de
software para el frontend:

1. Demostración de las capacidades en HTML y CSS
2. Demostración de las capacidades en JavaScript

### PARTE 1: DESARROLLO EN HTML Y CSS

![Screen](https://gitlab.com/hogar24-challenges/frontend-challenge/raw/master/assets/modelo_frontend.png)

* Crear un nuevo layout basado en la captura dada.
* Añadir hojas de estilo que cumplan a cabalidad el diseño.
* Si tiene conocimientos, añadir un preprocesador de código CSS. *Esto añadirá puntos adicionales*

### PARTE 2: Javascript

* Consumir la API https://api.nestoria.co.uk/api?encoding=json&pretty=1&action=search_listings&country=uk&listing_type=buy&place_name=brighton
para adquirir una lista de propiedades disponibles
* Mostrar los diez primeros anuncios del resultado de la API en el layout creado en la parte 1.
* Por favor usar alguno de los frameworks disponibles (React, AngularJS, VueJS, Ember, Etc.) y por favor justificar por qué hizo uso de ese framework.

## Instrucciones adicionales

* Por favor haga fork a este repositorio
* Tras finalizar, compartir la URL del repositorio con nosotros, puede ser con cualquier proveedor (Gitlab, Github, etc.)
* Los comentarios en el código son esenciales, por favor procurar mantener todo comentado.
* El readme de su proyecto es necesario y fundamental.
* Usar especificación ES6 y superior

## Puntos Adicionales

* Tests
* Código limpio y bien estructurado
* Aplicar las mejores prácticas y patrones de ingeniería del software.
* Uso de frameworks.
* Las medidas de pixeles son perfectas en el diseño.
* Creación y uso de componentes.
* Uso de Lint.
* Uso de conceptos de material design.
